package gcp

import (
	"context"
	"fmt"

	"google.golang.org/api/option"

	sm "cloud.google.com/go/secretmanager/apiv1"
	pb "cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
)

type Client struct {
	projectID string
	client    *sm.Client
}

func NewSMClient(projectID, keyFilePath string) (*Client, error) {

	var ops = make([]option.ClientOption, 0)
	if keyFilePath != "" {
		ops = append(ops, option.WithCredentialsFile(keyFilePath))
	}
	client, err := sm.NewClient(context.Background(), ops...)
	if err != nil {
		return nil, err
	}

	return &Client{
		projectID: projectID,
		client:    client,
	}, nil
}

func (c *Client) GetSecret(secretName string) (string, error) {

	// Build the request.
	accessRequest := &pb.AccessSecretVersionRequest{
		Name: fmt.Sprintf("projects/%v/secrets/%v/versions/%v", c.projectID, secretName, "latest"),
	}
	// Call the API.
	result, err := c.client.AccessSecretVersion(context.Background(), accessRequest)
	if err != nil {
		return "", err
	}

	return string(result.Payload.Data), nil
}
