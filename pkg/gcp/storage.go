package gcp

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

type StorageClient struct {
	client          *storage.Client
	projectID       string
	bucketName      string
	uploadPath      string
	uploadPubicPath string
	ctx             context.Context
	bucket          *storage.BucketHandle
	w               io.Writer
}

func NewStorageClient(projectID, bucketName, uploadPath, uploadPubicPath, keyFilePath string) *StorageClient {
	ctx := context.Background()
	var ops = make([]option.ClientOption, 0)
	if keyFilePath != "" {
		ops = append(ops, option.WithCredentialsFile(keyFilePath))
	}

	client, err := storage.NewClient(ctx, ops...)
	if err != nil {
		log.Fatalf("Failed to create storage client: %v", err)
	}

	return &StorageClient{
		client:          client,
		projectID:       projectID,
		bucketName:      bucketName,
		uploadPath:      uploadPath,
		uploadPubicPath: uploadPubicPath,
		ctx:             ctx,
		w:               &bytes.Buffer{},
		bucket:          client.Bucket(bucketName),
	}
}

func (s *StorageClient) UploadKYCFile(fileName string, data []byte) (urlFile string, err error) {
	log.Printf("Uploading file %s to bucket %s", fileName, s.bucketName)
	ctx, cancel := context.WithTimeout(s.ctx, time.Second*50)
	defer cancel()

	wc := s.bucket.Object(s.uploadPath + fileName).NewWriter(ctx)
	wc.ContentType = "image/jpeg"

	if _, err = wc.Write(data); err != nil {
		return "", fmt.Errorf("error writing file to gcp storage [%v]-[%s]: %w ", s.bucket, fileName, err)
	}
	if err := wc.Close(); err != nil {
		return "", fmt.Errorf("error uploading file to gcp storage [%v]-[%s]: %w ", s.bucket, fileName, err)
	}
	return s.getUrlFile(s.uploadPath + fileName)
}

func (s *StorageClient) UploadPublicImage(fileName string, data []byte) (urlFile string, err error) {
	fullName := fmt.Sprintf("%s/%s", s.uploadPubicPath, fileName)
	log.Printf("Uploading public image %s to bucket %s", fullName, s.bucketName)
	ctx, cancel := context.WithTimeout(s.ctx, time.Second*50)
	defer cancel()

	wc := s.bucket.Object(fullName).NewWriter(ctx)
	wc.ContentType = "image/jpeg"
	if _, err = wc.Write(data); err != nil {
		return "", fmt.Errorf("error writing file to gcp storage: %w", err)
	}
	if err := wc.Close(); err != nil {
		return "", fmt.Errorf("error uploading file to gcp storage [%v]-[%s]: %w", s.bucket, fullName, err)
	}

	acl := s.bucket.Object(fullName).ACL()
	if err := acl.Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
		return "", fmt.Errorf("error making public file in gcp storage[%v]-[%s]: %w", s.bucket, fullName, err)
	}
	return getPublicUrl(s.bucketName, fullName), nil
}

func (s *StorageClient) UploadPublicZip(fileName string, data []byte) (urlFile string, err error) {
	fullName := fmt.Sprintf("%s/%s", s.uploadPubicPath, fileName)
	log.Printf("Uploading public image %s to bucket %s", fullName, s.bucketName)
	ctx, cancel := context.WithTimeout(s.ctx, time.Second*50)
	defer cancel()

	wc := s.bucket.Object(fullName).NewWriter(ctx)
	wc.ContentType = "application/zip"
	if _, err = wc.Write(data); err != nil {
		return "", fmt.Errorf("error writing file to gcp storage: %w", err)
	}
	if err := wc.Close(); err != nil {
		return "", fmt.Errorf("error uploading file to gcp storage [%v]-[%s]: %w", s.bucket, fullName, err)
	}

	acl := s.bucket.Object(fullName).ACL()
	if err := acl.Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
		return "", fmt.Errorf("error making public file in gcp storage[%v]-[%s]: %w", s.bucket, fullName, err)
	}
	return getPublicUrl(s.bucketName, fullName), nil
}

func (s *StorageClient) getUrlFile(fileName string) (string, error) {
	_, err := io.WriteString(s.w, "\nFile stat:\n")
	if err != nil {
		return "", fmt.Errorf("error writing to buffer when gets image url[%s]: %w", fileName, err)
	}

	obj, err := s.bucket.Object(fileName).Attrs(s.ctx)
	if err != nil {
		return "", fmt.Errorf("failed to stat file[%s]: %w", fileName, err)
	}
	return obj.MediaLink, nil
}

func getPublicUrl(bucketName, fileName string) string {
	return fmt.Sprintf("https://storage.googleapis.com/%s/%s", bucketName, fileName)
}

func (s *StorageClient) UploadProtectedPublicZipFile(fileName, data []byte) (urlFile string, err error) {
	fullName := fmt.Sprintf("%s/%s", s.uploadPubicPath, fileName)
	log.Printf("Uploading public zip file %s to bucket %s", fullName, s.bucketName)
	ctx, cancel := context.WithTimeout(s.ctx, time.Second*50)
	defer cancel()

	wc := s.bucket.Object(fullName).NewWriter(ctx)
	wc.ContentType = "application/zip"
	if _, err = wc.Write(data); err != nil {
		return "", fmt.Errorf("error writing file to gcp storage[%v]-[%s]: %w", s.bucket, fullName, err)
	}
	if err := wc.Close(); err != nil {
		return "", fmt.Errorf("error uploading file to gcp storage[%v]-[%s]: %w", s.bucket, fullName, err)
	}

	acl := s.bucket.Object(fullName).ACL()
	if err := acl.Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
		return "", fmt.Errorf("error making public file in gcp storage[%v]-[%s]: %w", s.bucket, fullName, err)
	}
	return getPublicUrl(s.bucketName, fullName), nil
}
