package secret_manager

import (
	sm "cloud.google.com/go/secretmanager/apiv1"
	pb "cloud.google.com/go/secretmanager/apiv1/secretmanagerpb"
	"context"
	"encoding/json"
	"fmt"
	"github.com/cockroachdb/errors"
	"google.golang.org/api/option"
)

type Client struct {
	projectID  string
	apiKeysMap map[string]map[string][]string // cache for api keys indexed with secret key
	client     *sm.Client
}

func New(ctx context.Context, projectID, keyFilePath string) (*Client, error) {
	var ops = make([]option.ClientOption, 0)
	if keyFilePath != "" {
		ops = append(ops, option.WithCredentialsFile(keyFilePath))
	}
	client, err := sm.NewClient(ctx, ops...)
	if err != nil {
		return nil, errors.Wrapf(err, "error creating new secret manager client for project ID [%s] and key file [%v]", projectID, keyFilePath)
	}

	return &Client{
		projectID:  projectID,
		client:     client,
		apiKeysMap: make(map[string]map[string][]string, 0),
	}, nil
}

// GetRemoteSecret retrieves a secret from Google Secret Manager.
// It takes a secret name and uses "latest" version and returns the secret value and an error, if any.
func (c *Client) GetRemoteSecret(ctx context.Context, secretKey string) (string, error) {
	return c.GetSecretWithVersion(ctx, secretKey, "latest", false)
}

// GetSecret retrieves a secret from Google Secret Manager.
// It takes a secret name and uses "latest" version and returns the secret value and an error, if any.
// If is local is true, it returns the same value of `secretKey` parameter.
func (c *Client) GetSecret(ctx context.Context, secretKey string, isLocal bool) (string, error) {
	return c.GetSecretWithVersion(ctx, secretKey, "latest", isLocal)
}

// GetRemoteSecretWithVersion retrieves a secret from Google Secret Manager.
// It takes a secret name and a version and returns the secret value and an error, if any.
func (c *Client) GetRemoteSecretWithVersion(ctx context.Context, secretKey string, version string) (string, error) {
	return c.GetSecretWithVersion(ctx, secretKey, version, false)
}

// GetSecretWithVersion retrieves a secret from Google Secret Manager.
// It takes a secret name and a version and returns the secret value and an error, if any.
// If is local is true, it returns the same value of `secretKey` parameter.
func (c *Client) GetSecretWithVersion(ctx context.Context, secretKey string, version string, isLocal bool) (string, error) {
	if isLocal {
		return secretKey, nil
	}
	// Build the request.
	name := fmt.Sprintf("projects/%s/secrets/%s/versions/%s", c.projectID, secretKey, version)
	accessRequest := &pb.AccessSecretVersionRequest{
		Name: name,
	}
	// Call the API.
	result, err := c.client.AccessSecretVersion(ctx, accessRequest)
	if err != nil {
		return "", errors.Wrapf(err, "error obtaining secret [%s]", name)
	}

	return string(result.Payload.Data), nil
}

// GetServiceApiKeysV1 returns all valid API keys for given service name searching in secret key.
// If local is true returns the same value of `secretKey` parameter.
func (c *Client) GetServiceApiKeysV1(ctx context.Context, secretKey, serviceName string, isLocal bool) ([]string, error) {
	if isLocal {
		return []string{secretKey}, nil
	}

	apiKeys, err := c.getApiKeys(ctx, secretKey, isLocal)
	if err != nil {
		return nil, errors.Wrapf(err, "error obtaining api keys")
	}
	// searching for given service name in api keys
	for key, val := range apiKeys {
		if key != serviceName {
			continue
		}
		return val, nil
	}
	return nil, errors.Newf("api key for [%s] in key [%s] was not found", serviceName, secretKey)
}

// GetAnyValidServiceApiKeyV1 returns first valid API keys for given service name searching in secret key.
// If local is true returns the same value of `secretKey` parameter.
func (c *Client) GetAnyValidServiceApiKeyV1(ctx context.Context, secretKey, serviceName string, isLocal bool) (string, error) {
	apiKeys, err := c.GetServiceApiKeysV1(ctx, secretKey, serviceName, isLocal)
	if err != nil {
		return "", err
	}
	return apiKeys[0], nil
}

// Close closes the connection to the API server.
func (c *Client) Close() error {
	return c.client.Close()
}

func (c *Client) getApiKeys(ctx context.Context, secretKey string, isLocal bool) (map[string][]string, error) {
	// Populating api keys if nil
	apiKeys, found := c.apiKeysMap[secretKey]
	if !found {
		secretJsonContent, err := c.GetSecret(ctx, secretKey, isLocal)
		if err != nil {
			return nil, errors.Wrapf(err, "error getting services api keys in key [%s]", secretKey)
		}

		err = json.Unmarshal([]byte(secretJsonContent), &apiKeys)
		if err != nil {
			return nil, errors.Wrapf(err, "error unmarshalling services api keys in key [%s]", secretKey)
		}
		c.apiKeysMap[secretKey] = apiKeys
	}
	return apiKeys, nil
}
