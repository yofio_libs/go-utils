package money

import (
	"math"

	"github.com/Rhymond/go-money"
)

// NewMXNMoney returns a new *money.Money value with the amount provided.
func NewMXN(amount int64) *money.Money {
	return money.New(amount, "MXN")
}

func NewMXNMoneyFloat(amount float64) *money.Money {
	return NewMXN(int64(math.Round(amount * 100)))
}

func NewFloat64FromMoney(m *money.Money) *float64 {
	if m == nil {
		return nil
	}
	f := m.AsMajorUnits()
	return &f
}

func Float64FromMoney(m *money.Money) float64 {
   if m == nil {
      return 0
   }
   return m.AsMajorUnits()
}

// RoundOn rounds the val param with the decimals places number provided, with the on param provided.
// Ej. val = 150.045, on = 5, places = 2, returns 150.04.
func RoundOn(val float64, on float64, places int) float64 {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= on {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	return round / pow
}

func Add(values ...*money.Money) (res *money.Money, err error) {
	res = NewMXN(0)
	for _, v := range values {
		if v == nil {
			continue
		}
		res, err = res.Add(v)
		if err != nil {
			return
		}
	}
	return
}
