package date

import (
	"fmt"
	"reflect"
	"strings"
	"time"
)

var spanishMonths = []string{"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"}
var spanishWeekDays = []string{"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"}

const (
	WeekdayMonday    Weekday = "MONDAY"
	WeekdayTuesday   Weekday = "TUESDAY"
	WeekdayWednesday Weekday = "WEDNESDAY"
	WeekdayThursday  Weekday = "THURSDAY"
	WeekdayFriday    Weekday = "FRIDAY"
	WeekdaySaturday  Weekday = "SATURDAY"
	WeekdaySunday    Weekday = "SUNDAY"
)

type Weekday string

var weekdays = map[Weekday]time.Weekday{
	WeekdayMonday:    time.Monday,
	WeekdayTuesday:   time.Tuesday,
	WeekdayWednesday: time.Wednesday,
	WeekdayThursday:  time.Thursday,
	WeekdayFriday:    time.Friday,
	WeekdaySaturday:  time.Saturday,
	WeekdaySunday:    time.Sunday,
}

func Now() time.Time {
	location, err := time.LoadLocation("America/Mexico_City")
	if err != nil {
		panic(err)
	}
	return time.Now().In(location)
}

func DateNoTime(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func DateEndDate(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 23, 59, 59, 999, t.Location())
}

func (w Weekday) Valid() (err error) {
	switch Weekday(strings.ToUpper(string(w))) {
	case WeekdayMonday, WeekdayTuesday, WeekdayWednesday, WeekdayThursday, WeekdayFriday, WeekdaySaturday, WeekdaySunday:
		// Ok
	default:
		err = fmt.Errorf("invalid weekday name: value don't match with any actual weekday name [%s]", w)
	}
	return
}

func (w Weekday) ToTimeWeekday() (tW time.Weekday, err error) {
	tW, ok := weekdays[w]
	if !ok {
		err = fmt.Errorf("invalid weekday name: value don't match with any time.Weekday [%s]", w)
	}
	return
}

func Age(birthdate, now time.Time) (age int, err error) {
	if birthdate.IsZero() {
		err = fmt.Errorf("invalid birthdate: birthdate is zero")
		return
	}
	if now.IsZero() {
		err = fmt.Errorf("invalid now date: now is zero")
		return
	}

	ty, tm, td := now.Date()
	now = time.Date(ty, tm, td, 0, 0, 0, 0, now.Location())
	by, bm, bd := birthdate.Date()
	birthdate = time.Date(by, bm, bd, 0, 0, 0, 0, now.Location())
	if now.Before(birthdate) {
		return
	}
	age = ty - by
	anniversary := birthdate.AddDate(age, 0, 0)
	if anniversary.After(now) {
		age--
	}
	return
}

// GetMonthEs returns the month name in spanish like "Enero"
func GetMonthEs(month time.Month) (monthDayInSpanish string, err error) {
	if !isValidMonth(month) {
		err = fmt.Errorf("invalid month: value don't match with any actual month name [%s]", month)
		return
	}
	return spanishMonths[month-1], nil
}

// GetWeekdayEs returns the weekday name in spanish like "Lunes"
func GetWeekDayEs(weekday time.Weekday) (weekDayInSpanish string, err error) {
	if !isValidWeekDay(weekday) {
		return "", fmt.Errorf("invalid weekday: %s", weekday)
	}

	return spanishWeekDays[weekday], nil
}

func isValidWeekDay(t interface{}) bool {
	return reflect.DeepEqual(t, time.Sunday) ||
		reflect.DeepEqual(t, time.Monday) ||
		reflect.DeepEqual(t, time.Tuesday) ||
		reflect.DeepEqual(t, time.Wednesday) ||
		reflect.DeepEqual(t, time.Thursday) ||
		reflect.DeepEqual(t, time.Friday) ||
		reflect.DeepEqual(t, time.Saturday)
}

func isValidMonth(t interface{}) bool {
	return reflect.DeepEqual(t, time.January) ||
		reflect.DeepEqual(t, time.February) ||
		reflect.DeepEqual(t, time.March) ||
		reflect.DeepEqual(t, time.April) ||
		reflect.DeepEqual(t, time.May) ||
		reflect.DeepEqual(t, time.June) ||
		reflect.DeepEqual(t, time.July) ||
		reflect.DeepEqual(t, time.August) ||
		reflect.DeepEqual(t, time.September) ||
		reflect.DeepEqual(t, time.October) ||
		reflect.DeepEqual(t, time.November) ||
		reflect.DeepEqual(t, time.December)
}
