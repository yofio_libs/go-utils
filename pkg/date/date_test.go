package date

import (
	"testing"
	"time"
)

func TestAge(t *testing.T) {
	now := Now()

	type args struct {
		birthdate time.Time
		now       time.Time
	}
	tests := []struct {
		name    string
		args    args
		wantAge int
		wantErr bool
	}{
		{
			name: "Given invalid zero birthdate When calculating age Then error",
			args: args{
				now: now,
			},
			wantErr: true,
		},
		{
			name: "Given invalid zero now date When calculating age Then error",
			args: args{
				birthdate: now.AddDate(-30, 0, 0),
			},
			wantErr: true,
		},
		{
			name: "Given 30 years ago birthdate When calculating age Then success",
			args: args{
				birthdate: now.AddDate(-30, 0, 0),
				now:       now,
			},
			wantAge: 30,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotAge, err := Age(tt.args.birthdate, tt.args.now)
			if (err != nil) != tt.wantErr {
				t.Errorf("Age() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotAge != tt.wantAge {
				t.Errorf("Age() = %v, want %v", gotAge, tt.wantAge)
			}
		})
	}
}

func Test_GetMonthEs(t *testing.T) {
	type args struct {
		month time.Month
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Given invalid month When getting spanish month Then error",
			args: args{
				month: 13,
			},
			wantErr: true,
		},
		{
			name: "Given valid month January When getting spanish month Then success",
			args: args{
				month: time.January,
			},
			want: "Enero",
		},
		{
			name: "Given valid month February When getting spanish month Then success",
			args: args{
				month: time.February,
			},
			want: "Febrero",
		},
		{
			name: "Given valid month March When getting spanish month Then success",
			args: args{
				month: time.March,
			},
			want: "Marzo",
		},
		{
			name: "Given valid month April When getting spanish month Then success",
			args: args{
				month: time.April,
			},
			want: "Abril",
		},
		{
			name: "Given valid month May When getting spanish month Then success",
			args: args{
				month: time.May,
			},
			want: "Mayo",
		},
		{
			name: "Given valid month June When getting spanish month Then success",
			args: args{
				month: time.June,
			},
			want: "Junio",
		},
		{
			name: "Given valid month July When getting spanish month Then success",
			args: args{
				month: time.July,
			},
			want: "Julio",
		},
		{
			name: "Given valid month August When getting spanish month Then success",
			args: args{
				month: time.August,
			},
			want: "Agosto",
		},
		{
			name: "Given valid month September When getting spanish month Then success",
			args: args{
				month: time.September,
			},
			want: "Septiembre",
		},
		{
			name: "Given valid month October When getting spanish month Then success",
			args: args{
				month: time.October,
			},
			want: "Octubre",
		},
		{
			name: "Given valid month November When getting spanish month Then success",
			args: args{
				month: time.November,
			},
			want: "Noviembre",
		},
		{
			name: "Given valid month December When getting spanish month Then success",
			args: args{
				month: time.December,
			},
			want: "Diciembre",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetMonthEs(tt.args.month)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMonthEs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetMonthEs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_GetWeekDayEs(t *testing.T) {
	type args struct {
		weekday time.Weekday
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Given valid weekday 0 When getting spanish weekday Then success",
			args: args{
				weekday: time.Sunday,
			},
			want: "Domingo",
		},
		{
			name: "Given valid weekday 1 When getting spanish weekday Then success",
			args: args{
				weekday: time.Monday,
			},
			want: "Lunes",
		},
		{
			name: "Given valid weekday 2 When getting spanish weekday Then success",
			args: args{
				weekday: time.Tuesday,
			},
			want: "Martes",
		},
		{
			name: "Given valid weekday 3 When getting spanish weekday Then success",
			args: args{
				weekday: time.Wednesday,
			},
			want: "Miércoles",
		},
		{
			name: "Given valid weekday 4 When getting spanish weekday Then success",
			args: args{
				weekday: time.Thursday,
			},
			want: "Jueves",
		},
		{
			name: "Given valid weekday 5 When getting spanish weekday Then success",
			args: args{
				weekday: time.Friday,
			},
			want: "Viernes",
		},
		{
			name: "Given valid weekday 6 When getting spanish weekday Then success",
			args: args{
				weekday: time.Saturday,
			},
			want: "Sábado",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetWeekDayEs(tt.args.weekday)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetWeekDayEs() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetWeekDayEs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidMonth(t *testing.T) {
	type args struct {
		month time.Month
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Given valid month When validating month Then success",
			args: args{
				month: time.January,
			},
			want: true,
		},
		{
			name: "Given invalid month When validating month Then fail",
			args: args{
				month: 15,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidMonth(tt.args.month); got != tt.want {
				t.Errorf("isValidMonth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isValidWeekDay(t *testing.T) {
	type args struct {
		weekday time.Weekday
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Given valid weekday When validating weekday Then success",
			args: args{
				weekday: time.Sunday,
			},
			want: true,
		},
		{
			name: "Given invalid weekday When validating weekday Then fail",
			args: args{
				weekday: 8,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidWeekDay(tt.args.weekday); got != tt.want {
				t.Errorf("isValidWeekDay() = %v, want %v", got, tt.want)
			}
		})
	}
}
