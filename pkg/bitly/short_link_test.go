package bitly

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShortLink(t *testing.T) {
	apiKey := os.Getenv("BITLY_API_KEY")
	if apiKey == "" {
		t.Skip("BITLY_API_KEY is not set")
	}
	t.Run("Success should return a short link", func(t *testing.T) {
		// Given
		apiKey := apiKey
		url := "https://api-ssl.bitly.com/v4/bitlinks"
		domain := "bit.ly"
		longUrl := "https://dev.bitly.com"
		tags := []string{"yofio", "qr"}
		expectedShortUrl := "https://bitly.is/3mv85gN"

		bitly := NewBitlyService(apiKey, url, domain)

		shortUrl, err := bitly.ShortLink(longUrl, tags...)
		if err != nil {
			t.Errorf("error should be nil, got [%v]", err)
		}

		assert.Equal(t, expectedShortUrl, shortUrl)
	})

	t.Run("Error should return an error for invalid ", func(t *testing.T) {
		apiKey := apiKey
		url := "https://api-ssl.bitly.com/v4/bitlinks"
		domain := "yofio.co"
		longUrl := "https://dev.bitly.com"
		tags := []string{"yofio", "qr"}
		expectedShortUrl := ""

		bitly := NewBitlyService(apiKey, url, domain)

		shortUrl, err := bitly.ShortLink(longUrl, tags...)
		if err == nil {
			t.Errorf("error should not be nil, got [%v]", err)
		}

		assert.Equal(t, expectedShortUrl, shortUrl)
	})
}
