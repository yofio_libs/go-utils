package bitly

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type BitlyShorter interface {
	ShortLink(longUrl string, tags ...string) (string, error)
}

type bitlyService struct {
	client *http.Client
	apiKey string
	url    string
	domain string
}

type bitlyShortRequest struct {
	LongUrl string   `json:"long_url"`
	Domain  string   `json:"domain"`
	Tags    []string `json:"tags"`
}

type bitlyShortResponse struct {
	Link string `json:"link"`
}

type bitlyErrorResponse struct {
	Message string `json:"message"`
}

func NewBitlyService(apiKey, url, domain string) BitlyShorter {
	return &bitlyService{
		client: &http.Client{
			Timeout: time.Second * 60,
		},
		domain: domain,
		apiKey: apiKey,
		url:    url,
	}
}

func (b bitlyService) ShortLink(longUrl string, tags ...string) (string, error) {
	reqBody := bitlyShortRequest{
		LongUrl: longUrl,
		Domain:  b.domain,
		Tags:    tags,
	}

	body, _ := json.Marshal(reqBody)

	req, err := http.NewRequest("POST", b.url, bytes.NewBuffer(body))
	if err != nil {
		return "", err
	}

	req.Header.Add("Authorization", "Bearer "+b.apiKey)

	resp, err := b.client.Do(req)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 201 && resp.StatusCode != 200 {
		var errResp bitlyErrorResponse
		err = json.NewDecoder(resp.Body).Decode(&errResp)
		if err != nil {
			return "", err
		}
		return "", fmt.Errorf("bitly status: %s. bitly message %s", resp.Status, errResp.Message)
	}

	var shortResp bitlyShortResponse
	err = json.NewDecoder(resp.Body).Decode(&shortResp)
	if err != nil {
		return "", err
	}

	return shortResp.Link, nil
}
