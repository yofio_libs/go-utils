package database

import (
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSetNullStringArray(t *testing.T) {
	tests := []struct {
		name   string
		params []string
		want   []sql.NullString
	}{
		{
			name:   "Convert string array to sqlnullstring array successfully with 2 elements",
			params: []string{"first", "second"},
			want:   []sql.NullString{{String: "first", Valid: true}, {String: "second", Valid: true}},
		},
		{
			name:   "Convert string array to sqlnullstring array successfully with 0 elements",
			params: []string{},
			want:   []sql.NullString{},
		},
		{
			name:   "Convert string array to sqlnullstring array successfully with null elements",
			params: []string{"", ""},
			want:   []sql.NullString{{String: "", Valid: false}, {String: "", Valid: false}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SetNullStringArray(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGetNullStringArray(t *testing.T) {
	tests := []struct {
		name   string
		params []sql.NullString
		want   []string
	}{
		{
			name:   "Convert string array to sqlnullstring array successfully with 2 elements",
			params: []sql.NullString{{String: "first", Valid: true}, {String: "second", Valid: true}},
			want:   []string{"first", "second"},
		},
		{
			name:   "Convert string array to sqlnullstring array successfully with 0 elements",
			params: []sql.NullString{},
			want:   []string{},
		},
		{
			name:   "Convert string array to sqlnullstring array successfully with null elements",
			params: []sql.NullString{{String: "", Valid: false}, {String: "", Valid: false}},
			want:   []string{"", ""},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetNullStringArray(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}
func TestGetNullInt32Ptr(t *testing.T) {
	tests := []struct {
		name   string
		params *int32
		want   sql.NullInt32
	}{
		{
			name:   "Convert a basic number int to sql.NullInt32 valid",
			params: convertInt32ToPtr(32),
			want: sql.NullInt32{
				Int32: 32,
				Valid: true,
			},
		},
		{
			name:   "Convert a zero to sql.NullInt32 valid",
			params: convertInt32ToPtr(0),
			want: sql.NullInt32{
				Int32: 0,
				Valid: true,
			},
		},
		{
			name:   "Convert a Null int to sql.NullInt32 no valid",
			params: nil,
			want: sql.NullInt32{
				Int32: 0,
				Valid: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SetNullInt32Ptr(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}
func TestGetNullInt64Ptr(t *testing.T) {
	tests := []struct {
		name   string
		params *int64
		want   sql.NullInt64
	}{
		{
			name:   "Convert a basic number int to sql.NullInt64 valid",
			params: convertInt64ToPtr(32),
			want: sql.NullInt64{
				Int64: 32,
				Valid: true,
			},
		},
		{
			name:   "Convert a zero to sql.NullInt64 valid",
			params: convertInt64ToPtr(0),
			want: sql.NullInt64{
				Int64: 0,
				Valid: true,
			},
		},
		{
			name:   "Convert a Null int to sql.NullInt64 no valid",
			params: nil,
			want: sql.NullInt64{
				Int64: 0,
				Valid: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SetNullInt64Ptr(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}
func TestGetNullFloat64Ptr(t *testing.T) {
	tests := []struct {
		name   string
		params *float64
		want   sql.NullFloat64
	}{
		{
			name:   "Convert a basic number int to sql.NullFloat64 valid",
			params: convertFloat64ToPtr(32),
			want: sql.NullFloat64{
				Float64: 32,
				Valid:   true,
			},
		},
		{
			name:   "Convert a zero to sql.NullFloat64 valid",
			params: convertFloat64ToPtr(0),
			want: sql.NullFloat64{
				Float64: 0,
				Valid:   true,
			},
		},
		{
			name:   "Convert a Null int to sql.NullFloat64 no valid",
			params: nil,
			want: sql.NullFloat64{
				Float64: 0,
				Valid:   false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SetNullFloat64Ptr(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}
func TestGetNullBool64Ptr(t *testing.T) {
	tests := []struct {
		name   string
		params *bool
		want   sql.NullBool
	}{
		{
			name:   "Convert a basic boolean to sql.NullBool valid",
			params: convertBoolToPtr(true),
			want: sql.NullBool{
				Bool:  true,
				Valid: true,
			},
		},
		{
			name:   "Convert a zero to sql.NullBool valid",
			params: convertBoolToPtr(false),
			want: sql.NullBool{
				Bool:  false,
				Valid: true,
			},
		},
		{
			name:   "Convert a Null int to sql.NullBool no valid",
			params: nil,
			want: sql.NullBool{
				Bool:  false,
				Valid: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SetNullBoolPtr(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}
func TestGetNullTimerPtr(t *testing.T) {
	tests := []struct {
		name   string
		params sql.NullTime
		want   *time.Time
	}{
		{
			name: "Convert null to timer pointer",
			params: sql.NullTime{
				Time:  time.Time{},
				Valid: false,
			},
			want: nil,
		},
		{
			name: "Convert valid date to timer pointer with some value",
			params: sql.NullTime{
				Time:  time.Time{},
				Valid: true,
			},
			want: convertTimeToPtr(time.Time{}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetNullTimePtr(tt.params)
			assert.Equal(t, tt.want, got)
		})
	}
}

// Functions for unit testing only

func convertInt32ToPtr(number int32) *int32 {
	return &number
}

func convertInt64ToPtr(number int64) *int64 {
	return &number
}

func convertFloat64ToPtr(number float64) *float64 {
	return &number
}

func convertBoolToPtr(boolean bool) *bool {
	return &boolean
}

func convertTimeToPtr(time time.Time) *time.Time {
	return &time
}
