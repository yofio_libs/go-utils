package database

import (
	"database/sql"
	"time"

	"github.com/Rhymond/go-money"
	yofioMoney "gitlab.com/yofio_libs/go-utils/pkg/money"
)

func SetNullFloat64(f float64) sql.NullFloat64 {
	nf := sql.NullFloat64{
		Float64: f,
		Valid:   f != 0,
	}
	return nf
}

func SetNullInt32(i int32) sql.NullInt32 {
	ni := sql.NullInt32{
		Int32: i,
		Valid: i != 0,
	}
	return ni
}

func SetNullInt64(i int64) sql.NullInt64 {
	ni := sql.NullInt64{
		Int64: i,
		Valid: i != 0,
	}
	return ni
}

func GetNullInt32(ns sql.NullInt32) int32 {
	if !ns.Valid {
		return 0
	}
	return ns.Int32
}

func GetNullInt64(ns sql.NullInt64) int64 {
	if !ns.Valid {
		return 0
	}
	return ns.Int64
}

// With this function you can set a null or float64 to the DB
func SetNullFloat64Ptr(f *float64) sql.NullFloat64 {
	var nf sql.NullFloat64
	if f != nil {
		nf = sql.NullFloat64{
			Float64: *f,
			Valid:   true,
		}
		return nf
	}
	return nf
}

// With this function you can set a null or int32 to the DB
func SetNullInt32Ptr(i *int32) sql.NullInt32 {
	var ni sql.NullInt32
	if i != nil {
		ni = sql.NullInt32{
			Int32: *i,
			Valid: true,
		}
		return ni
	}
	return ni
}

// With this function you can set a null or int64 to the DB
func SetNullInt64Ptr(i *int64) sql.NullInt64 {
	var ni sql.NullInt64
	if i != nil {
		ni = sql.NullInt64{
			Int64: *i,
			Valid: true,
		}
		return ni
	}
	return ni
}

func SetNullMoney(m *money.Money) sql.NullFloat64 {
	nf := sql.NullFloat64{}
	if m != nil {
		nf = sql.NullFloat64{
			Float64: m.AsMajorUnits(),
			Valid:   true,
		}
	}
	return nf
}

func GetNullMoney(nf sql.NullFloat64) *money.Money {
	if !nf.Valid {
		return nil
	}
	return yofioMoney.NewMXNMoneyFloat(nf.Float64)
}

func SetNullString(str string) sql.NullString {
	ns := sql.NullString{}
	if str != "" {
		ns = sql.NullString{
			String: str,
			Valid:  true,
		}
	}
	return ns
}

// With this function you can set a null or string to the DB
func SetNullStringPtr(str *string) sql.NullString {
	var ns sql.NullString
	if str != nil {
		ns = sql.NullString{
			String: *str,
			Valid:  true,
		}
		return ns
	}
	return ns
}

func GetNullString(ns sql.NullString) string {
	if !ns.Valid {
		return ""
	}
	return ns.String
}

func SetNullTime(t time.Time) sql.NullTime {
	nt := sql.NullTime{}
	if t != nt.Time {
		nt = sql.NullTime{
			Time:  t,
			Valid: true,
		}
	}
	return nt
}

func GetNullTime(nt sql.NullTime) time.Time {
	if !nt.Valid {
		return time.Time{}
	}
	return nt.Time
}

func GetNullTimePtr(nt sql.NullTime) *time.Time {
	if !nt.Valid {
		return nil
	}
	return &nt.Time
}

func SetNullBool(bl bool) sql.NullBool {
	bln := sql.NullBool{
		Bool:  bl,
		Valid: true,
	}
	return bln
}
func SetNullBoolPtr(bl *bool) sql.NullBool {
	var bln sql.NullBool
	if bl != nil {
		bln = sql.NullBool{
			Bool:  *bl,
			Valid: true,
		}
	}
	return bln
}

func GetNullBool(bl sql.NullBool) bool {
	if !bl.Valid {
		return false
	}
	return bl.Bool
}

func SetNullStringArray(strs []string) []sql.NullString {
	ns := make([]sql.NullString, len(strs))
	for i, str := range strs {
		ns[i] = SetNullString(str)
	}
	return ns
}

func GetNullStringArray(ns []sql.NullString) []string {
	strs := make([]string, len(ns))
	for i, n := range ns {
		strs[i] = n.String
	}
	return strs
}
