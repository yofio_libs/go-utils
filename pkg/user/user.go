package user

import (
	"errors"
	"fmt"
	"math"
	"net"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"gitlab.com/yofio_libs/go-utils/pkg/date"
	yofioerror "gitlab.com/yofio_libs/go-utils/pkg/yofio_error"
)

func GetBirthdayByRFCOrCURP(s string) (birthday time.Time, err error) {
	err = ValidRFCFormat(s)
	if err == nil {
		birthday = getRFCOrCURPBirthday(s)
	} else if err = ValidCURPFormat(s); err == nil {
		birthday = getRFCOrCURPBirthday(s)
	} else {
		err = fmt.Errorf("invalid rfc or curp format: %s - err %s", s, err)
	}
	return
}

func ValidRFCFormat(s string) (err error) {
	if !regexp.MustCompile(`^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d]{1,2})$`).
		MatchString(s) {
		err = fmt.Errorf("invalid RFC format: %s", s)
	}
	return
}

func ValidCURPFormat(s string) (err error) {
	if len(s) != 18 {
		return fmt.Errorf("invalid CURP lenght: %s", s)
	}
	if !regexp.MustCompile(`^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$`).
		MatchString(s) {
		return fmt.Errorf("invalid CURP format: %s", s)
	}
	return validCURPCheckDigit(s)
}

func validCURPCheckDigit(curp string) (err error) {
	characteresDigitVerified := [...]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E",
		"F", "G", "H", "I", "J", "K", "L", "M", "N", "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

	splitCurp := strings.SplitAfter(curp[:len(curp)-1], "")
	values := [17]int{}
	var total int

	for indexCurp, letter := range splitCurp {
		for index, digit := range characteresDigitVerified {
			if letter == digit {
				values[indexCurp] = index
			}
		}
	}

	for k := 0; k < len(values); k++ {
		total += values[k] * (18 - k)
	}

	digit := 10 - math.Mod(float64(total), 10)
	_ = "12,10,10,19,8,8,0,2,0,3,17,30,29,29,12,32,1"

	if digit == 10 {
		digit = 0
	}

	if fmt.Sprintf("%.0f", digit) != string(curp[len(curp)-1]) {
		err = fmt.Errorf("invalid curp: invalid verification digit [%s]", curp)
	}
	return
}

func getRFCOrCURPBirthday(s string) time.Time {
	t, _ := time.ParseInLocation("060102", s[4:10], date.Now().Location())
	if t.After(date.Now()) {
		t = t.AddDate(-100, 0, 0)
	}
	return t
}

func ValidateEmailFormat(email string) error {
	if len(email) < 5 || len(email) > 100 {
		return yofioerror.NewInternalError(errors.New("email bad length"), yofioerror.InvalidEmailFormat, "por favor revisa el email que ingresaste [%s]", email)
	}

	_, err := mail.ParseAddress(email)
	if err != nil {
		return yofioerror.NewInternalError(err, yofioerror.InvalidEmailFormat, "por favor revisa el email que ingresaste [%s]", email)
	}

	parts := strings.Split(email, "@")
	_, err = net.LookupMX(parts[1])
	if err != nil {
		return yofioerror.NewInternalError(err, yofioerror.InvalidEmailDomain, "el email que ingresaste no existe, por favor revísalo nuevamente[%s]", email)
	}
	return nil
}

// GetPhoneNumberWithoutCountryCode returns the phone number without country code. Example: +525512345678 -> 5512345678
func GetPhoneNumberWithoutCountryCode(phoneNumber string) (string, error) {
	if len(phoneNumber) < 12 || len(phoneNumber) > 17 {
		return "", fmt.Errorf("invalid phone number")
	}
	return phoneNumber[len(phoneNumber)-10:], nil
}

func ClearTwilioPhoneNumber(phoneNumber string) (string, error) {
	if len(phoneNumber) < 12 {
		return "", fmt.Errorf("invalid phone number")
	}

	phoneNumber, err := deleteNonDigits(phoneNumber)
	if err != nil {
		return "", err
	}

	if len(phoneNumber) < 11 {
		return "", fmt.Errorf("invalid phone number")
	}

	re := regexp.MustCompile(`^+521`)

	correctedNumber := re.ReplaceAllString(phoneNumber, "+52")
	if correctedNumber[0] != '+' {
		correctedNumber = "+" + correctedNumber
	}
	return correctedNumber, nil
}
func deleteNonDigits(phoneNumber string) (string, error) {
	compiled, err := regexp.Compile(`[^\d]`)
	if err != nil {
		return "", err
	}
	return compiled.ReplaceAllString(phoneNumber, ""), nil
}
