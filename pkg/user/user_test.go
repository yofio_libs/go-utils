package user

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/yofio_libs/go-utils/pkg/date"
)

func TestValidRFCFormat(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Given invalid RFC When validating RFC format Then error",
			args: args{
				s: "<< INVALID RFC >>",
			},
			wantErr: true,
		},
		{
			name: "Given valid RFC When validating RFC format Then success",
			args: args{
				s: "PEBF220127GI7",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidRFCFormat(tt.args.s); (err != nil) != tt.wantErr {
				t.Errorf("ValidRFCFormat() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidCURPFormat(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Given invalid CURP When validating CURP format Then error",
			args: args{
				s: "<< INVALID CURP >>",
			},
			wantErr: true,
		},
		{
			name: "Given valid CURP When validating CURP format Then success",
			args: args{
				s: "PEBF220118HMSXHRA0",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidCURPFormat(tt.args.s); (err != nil) != tt.wantErr {
				t.Errorf("ValidCURPFormat() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidCURPCheckDigit(t *testing.T) {
	type args struct {
		curp string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Given invalid CURP digit When validating digit CURP Then error",
			args: args{
				curp: "PEBF211231HNTXHRA1",
			},
			wantErr: true,
		},
		{
			name: "Given valid CURP with non-zero or A character When validating digit CURP Then success",
			args: args{
				curp: "CAAJ880203HTSSCV13",
			},
		},
		{
			name: "Given valid CURP When validating digit CURP Then success",
			args: args{
				curp: "PEBF211231HNTXHRA0",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validCURPCheckDigit(tt.args.curp); (err != nil) != tt.wantErr {
				t.Errorf("ValidCURPCheckDigit() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestGetBirthdayByRFCOrCURP(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name         string
		args         args
		wantBirthday time.Time
		wantErr      bool
	}{
		{
			name: "Given invalid CURP When get birthday from curp Then error",
			args: args{
				s: "X",
			},
			wantBirthday: time.Time{},
			wantErr:      true,
		},
		{
			name: "Given valid CURP When get birthday from curp Then birthday",
			args: args{
				s: "CAAJ880203HTSSCV13",
			},
			wantBirthday: time.Date(1988, 2, 3, 0, 0, 0, 0, date.Now().Location()),
		},
		{
			name: "Given valid RFC When get birthday from rfc Then birthday",
			args: args{
				s: "PEBF220127GI7",
			},
			wantBirthday: time.Date(2022, 1, 27, 0, 0, 0, 0, date.Now().Location()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotBirthday, err := GetBirthdayByRFCOrCURP(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetBirthdayByRFCOrCURP() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotBirthday, tt.wantBirthday) {
				t.Errorf("GetBirthdayByRFCOrCURP() gotBirthday = %v, want %v", gotBirthday, tt.wantBirthday)
			}
		})
	}
}

func TestValidateEmailFormat(t *testing.T) {
	type args struct {
		email string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Given shortest email When validate email format Then error",
			args: args{
				email: "a@b",
			},
			wantErr: true,
		},
		{
			name: "Given invalid email address When validate email format Then error",
			args: args{
				email: "a.@b.com",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateEmailFormat(tt.args.email); (err != nil) != tt.wantErr {
				t.Errorf("ValidateEmailFormat() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_GetPhoneNumberWithoutCountryCode(t *testing.T) {
	type params struct {
		phoneNumber string
	}
	tests := []struct {
		name    string
		params  params
		want    string
		wantErr bool
	}{
		{
			name: "Given empty phonenumber When getting phone number without country code Then return error",
			params: params{
				phoneNumber: "",
			},
			wantErr: true,
		},
		{
			name: "Given invalid phonenumber When getting phone number without country code Then return error",
			params: params{
				phoneNumber: "123456789",
			},
			wantErr: true,
		},
		{
			name: "Given invalid phonenumber with more than 6 digits in the country code When getting phone number without country code Then return error",
			params: params{
				phoneNumber: "+12345671234567890",
			},
			wantErr: true,
		},
		{
			name: "Given valid phonenumber with 1 digits as country code When getting phone number without country code Then return OK",
			params: params{
				phoneNumber: "+11234567890",
			},
			want:    "1234567890",
			wantErr: false,
		},
		{
			name: "Given valid phonenumber with 2 digits as country code When getting phone number without country code Then return OK",
			params: params{
				phoneNumber: "+521234567890",
			},
			want:    "1234567890",
			wantErr: false,
		},
		{
			name: "Given valid phonenumber with 3 digits as country code When getting phone number without country code Then return OK",
			params: params{
				phoneNumber: "+5211234567890",
			},
			want:    "1234567890",
			wantErr: false,
		},
		{
			name: "Given valid phonenumber with 4 digits as country code When getting phone number without country code Then return OK",
			params: params{
				phoneNumber: "+13451234567890",
			},
			want:    "1234567890",
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetPhoneNumberWithoutCountryCode(tt.params.phoneNumber)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPhoneNumberWithoutCountryCode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetPhoneNumberWithoutCountryCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_clearTwilioPhoneNumber(t *testing.T) {
	tests := []struct {
		name        string
		phoneNumber string
		want        string
		wantErr     bool
	}{
		{
			name:        "should remove whatsapp from phone number",
			phoneNumber: "whatsapp:+5211234567890",
			want:        "+521234567890",
			wantErr:     false,
		},
		{
			name:        "should remove whatsapp from phone number",
			phoneNumber: "+5211234567890",
			want:        "+521234567890",
			wantErr:     false,
		},
		{
			name:        "should not remove whatsapp from phone number",
			phoneNumber: "+11234567890",
			want:        "+11234567890",
			wantErr:     false,
		},
		{
			name:        "should not remove whatsapp from phone number, error no valid phone number",
			phoneNumber: "+2323",
			want:        "",
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, err := ClearTwilioPhoneNumber(tt.phoneNumber); got != tt.want {
				if (err != nil) != tt.wantErr {
					t.Errorf("clearTwilioPhoneNumber() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
				t.Errorf("clearTwilioPhoneNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}
