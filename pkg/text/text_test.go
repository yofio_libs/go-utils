package text

import (
	"testing"

	"github.com/Rhymond/go-money"
	yofioMoney "gitlab.com/yofio_libs/go-utils/pkg/money"
)

func Test_amountToStringAndWords(t *testing.T) {
	type args struct {
		amounts []*money.Money
	}
	tests := []struct {
		name       string
		args       args
		wantErr    []bool
		wantString []string
		wantWords  []string
	}{
		{
			name: "Given correct amounts When translating amount is called Then sucess",
			args: args{
				amounts: []*money.Money{
					yofioMoney.NewMXNMoneyFloat(12332.64),
					yofioMoney.NewMXNMoneyFloat(100),
					yofioMoney.NewMXNMoneyFloat(104132023.78),
					yofioMoney.NewMXNMoneyFloat(0.78),
					yofioMoney.NewMXNMoneyFloat(-1023.78),
				},
			},
			wantErr: []bool{
				false,
				false,
				false,
				true,
				true,
			},
			wantString: []string{
				"12,332.64",
				"100.00",
				"104,132,023.78",
				"",
				"",
			},
			wantWords: []string{
				"(doce mil trescientos treinta y dos pesos con 64/100, moneda de curso legal en los Estados Unidos Mexicanos)",
				"(cien pesos con 00/100, moneda de curso legal en los Estados Unidos Mexicanos)",
				"(ciento cuatro millones ciento treinta y dos mil veinte y tres pesos con 78/100, moneda de curso legal en los Estados Unidos Mexicanos)",
				"",
				"",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i, a := range tt.args.amounts {
				toString, toWords, err := AmountToStringAndWords(a)
				if (err != nil) && !tt.wantErr[i] {
					t.Fatalf("%s amount %s error %v", tt.name, a.Display(), err)
					return
				}
				if tt.wantErr[i] {
					continue
				}
				if tt.wantString[i] != toString {
					t.Fatalf("%s amount %s want %s result %s", tt.name, a.Display(), tt.wantString[i], toString)
				}
				if tt.wantWords[i] != toWords {
					t.Fatalf("%s amount %s want %s result %s", tt.name, a.Display(), tt.wantWords[i], toWords)
				}
			}
		})
	}
}

func Test_interestToStringAndWords(t *testing.T) {
	type args struct {
		interests []float64
	}
	tests := []struct {
		name       string
		args       args
		wantErr    []bool
		wantString []string
		wantWords  []string
	}{
		{
			name: "Given interest When translating amount is called Then sucess or error",
			args: args{
				interests: []float64{
					0.23,
					2.50,
					85.01,
					103.20,
					-1.34,
				},
			},
			wantErr: []bool{
				false,
				false,
				false,
				true,
				true,
			},
			wantString: []string{
				"0.23",
				"2.50",
				"85.01",
				"",
				"",
			},
			wantWords: []string{
				"cero punto veinte y tres porciento",
				"dos punto cincuenta porciento",
				"ochenta y cinco punto uno porciento",
				"",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i, a := range tt.args.interests {
				toString, toWords, err := InterestToStringAndWords(a)
				if (err != nil) && !tt.wantErr[i] {
					t.Fatalf("%s interest %.2f error %v", tt.name, a, err)
					return
				}
				if tt.wantErr[i] {
					continue
				}
				if tt.wantString[i] != toString {
					t.Fatalf("%s interest %.2f want %s result %s", tt.name, a, tt.wantString[i], toString)
				}
				if tt.wantWords[i] != toWords {
					t.Fatalf("%s interest %.2f want %s result %s", tt.name, a, tt.wantWords[i], toWords)
				}
			}
		})
	}
}
