package text

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/Rhymond/go-money"
	yofioMoney "gitlab.com/yofio_libs/go-utils/pkg/money"
)

func getUnitsEs() []string {
	return []string{"un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve"}
}

func getFirstTenEs() []string {
	return []string{"once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve"}
}

func getTensEs() []string {
	return []string{"diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"}
}

func getHundredsEs() []string {
	return []string{"ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos"}
}

func AmountToStringAndWords(amount *money.Money) (toString, toWords string, err error) {
	fAmount := yofioMoney.Float64FromMoney(amount)
	if fAmount < 1 || fAmount >= 1000000000 {
		err = errors.New("error invalid amount, valid range 1.00 to 999,999,999.99 ")
		return
	}
	s := fmt.Sprintf("%.2f", fAmount)
	var i int
	for i = len(s) - 6; i >= 0; i -= 3 {
		toString = "," + s[i:i+3] + toString
	}
	if i != -3 {
		toString = s[0:3+i] + toString
	}
	toString = strings.TrimLeft(toString, ",")
	fracPart := s[len(s)-2:]
	toString = toString + s[len(s)-3:]
	intPart := int64(fAmount)
	millions := intPart / 1000000
	rest := intPart - millions*1000000
	thousands := rest / 1000
	hundreds := rest - thousands*1000
	if thousands != 0 {
		toWords = translate(thousands, "mil")
		if thousands == 1 {
			toWords = strings.Replace(toWords, "un mil", "mil", 1)
		}
	}
	if millions != 0 {
		toWords = translate(millions, "millones") + " " + toWords
		if millions == 1 {
			toWords = strings.Replace(toWords, "millones", "millón", 1)
		}
		toWords = strings.TrimSpace(toWords)
		if thousands == 0 && hundreds == 0 {
			toWords = toWords + " de"
		}
	}
	if hundreds != 0 {
		toWords = toWords + " " + translate(hundreds, "")
	}
	if intPart == 1 {
		toWords = "(" + strings.TrimSpace(toWords) + " peso con " + fracPart + "/100, moneda de curso legal en los Estados Unidos Mexicanos)"
	} else {
		toWords = "(" + strings.TrimSpace(toWords) + " pesos con " + fracPart + "/100, moneda de curso legal en los Estados Unidos Mexicanos)"
	}
	return
}

func InterestToStringAndWords(interest float64) (toString, toWords string, err error) {
	if interest < 0.01 || interest > 100 {
		err = errors.New("error invalid interest, valid range 0.01 to 99.99")
		return
	}
	toString = fmt.Sprintf("%.2f", interest)
	fracPart := toString[len(toString)-2:]
	hundreds := int64(interest)
	frac, _ := strconv.Atoi(fracPart)
	toWords = translate(hundreds, "punto ") + translate(int64(frac), "porciento")
	toWords = strings.Replace(toWords, "un punto", "uno punto", 1)
	toWords = strings.Replace(toWords, "un porciento", "uno porciento", 1)
	return
}

func translate(cant int64, unit string) (s string) {
	unidades := getUnitsEs()
	dieces := getFirstTenEs()
	decenas := getTensEs()
	centenas := getHundredsEs()
	c := cant / 100
	if c != 0 {
		if cant != 100 {
			s = centenas[c-1]
		} else {
			s = "cien"
		}
	}
	r := cant % 100
	d := r / 10
	if d != 0 {
		if d == 1 {
			r = r % 10
			if r == 0 {
				s = s + " " + decenas[0]
			} else {
				s = s + " " + dieces[r-1]
			}
			r = 0
		} else {
			s = s + " " + decenas[(d-1)%10]
		}
	}
	r = r % 10
	if r != 0 {
		if r != cant && cant%100 > 9 {
			s = s + " y " + unidades[r-1]
		} else {
			s = s + " " + unidades[r-1]
		}
	}
	if cant == 0 {
		s = s + " cero"
	}
	s = strings.TrimSpace(s) + " " + unit
	return
}

func MapEncode(mapValues map[string]string) (mapEncoded []byte, err error) {
	b := new(bytes.Buffer)
	e := gob.NewEncoder(b)
	err = e.Encode(mapValues)
	if err == nil {
		mapEncoded = b.Bytes()
	}
	return
}

func MapDecode(mapEncoded []byte) (mapValues map[string]string, err error) {
	b := bytes.NewReader(mapEncoded)
	d := gob.NewDecoder(b)
	err = d.Decode(&mapValues)
	return
}
