package jwt

import (
	"fmt"
	"log"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/yofio_libs/go-utils/pkg/date"
)

type JWT struct {
	privateKey []byte
	publicKey  []byte
}

func NewJWT(privKey []byte, pubKey []byte) JWT {
	return JWT{
		privateKey: privKey,
		publicKey:  pubKey,
	}
}

// Create a new JWT token. The token will be valid for 1 hour.
func (j JWT) Create(content interface{}, hoursLive int) (token string, expiresTimeStamp int64, err error) {
	key, err := jwt.ParseRSAPrivateKeyFromPEM(j.privateKey)
	if err != nil {
		return "", 0, fmt.Errorf("create: parse key: %w", err)
	}

	claims := make(jwt.MapClaims)
	expires := date.Now().Add(time.Hour * time.Duration(hoursLive)).Unix()

	claims["usr"] = content           // Our custom data.
	claims["exp"] = expires           // The expiration time after which the token must be disregarded.
	claims["iat"] = date.Now().Unix() // The time at which the token was issued.
	claims["nbf"] = date.Now().Unix() // The time before which the token must be disregarded.

	token, err = jwt.NewWithClaims(jwt.SigningMethodRS256, claims).SignedString(key)
	if err != nil {
		return "", 0, fmt.Errorf("create: sign token: %w", err)
	}

	return token, expires, nil
}

func (j JWT) Validate(token string) (interface{}, error) {
	key, err := jwt.ParseRSAPublicKeyFromPEM(j.publicKey)
	if err != nil {
		return "", fmt.Errorf("validate: parse key: %w", err)
	}

	tok, err := jwt.Parse(token, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", jwtToken.Header["alg"])
		}

		return key, nil
	})
	if err != nil {
		log.Printf("token to validate: %v", token)
		return nil, fmt.Errorf("validate token: %w", err)
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok || !tok.Valid {
		return nil, fmt.Errorf("validate: invalid")
	}

	return claims["usr"], nil
}
