package jwt

import (
	"encoding/json"
	"os"

	"testing"
)

func TestCreateJWT(t *testing.T) {
	type args struct {
		content   interface{}
		hoursLive int
		rsa       []byte
		pem       []byte
	}
	rsa, err := os.ReadFile("cert_test/id_rsa")
	if err != nil {
		t.Fatal(err)
	}
	pem, err := os.ReadFile("cert_test/id_rsa.pub")
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name          string
		args          args
		wantToken     string
		wantTimeStamp int64
		wantErr       bool
	}{
		{
			name: "CreateJWT success",
			args: args{
				content: map[string]interface{}{
					"user_id": "12345",
				},
				hoursLive: 1,
				rsa:       rsa,
				pem:       pem,
			},
			wantToken:     "token",
			wantTimeStamp: 10,
			wantErr:       false,
		},
		{
			name: "CreateJWT failed. Invalid rsa",
			args: args{
				content: map[string]interface{}{
					"user_id": "12345",
				},
				hoursLive: 1,
				rsa:       []byte("Bad rsa"),
				pem:       pem,
			},
			wantToken:     "",
			wantTimeStamp: 0,
			wantErr:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := NewJWT(tt.args.rsa, tt.args.pem)
			gotToken, gotTimeStamp, err := j.Create(tt.args.content, tt.args.hoursLive)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateJWT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(gotToken) < len(tt.wantToken) {
				t.Errorf("CreateJWT() gotToken = %v, want %v", gotToken, tt.wantToken)
			}
			if gotTimeStamp < tt.wantTimeStamp {
				t.Errorf("CreateJWT() gotTimeStamp = %v, want %v", gotTimeStamp, tt.wantTimeStamp)
			}
		})
	}
}

func TestValidateJWT(t *testing.T) {
	type args struct {
		rsa []byte
		pem []byte
	}
	rsa, err := os.ReadFile("cert_test/id_rsa")
	if err != nil {
		t.Fatal(err)
	}
	pem, err := os.ReadFile("cert_test/id_rsa.pub")
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "ValidateJWT success",
			args: args{
				rsa: rsa,
				pem: pem,
			},
			wantErr: false,
		},
		{
			name: "ValidateJWT failed. Invalid public key",
			args: args{
				rsa: rsa,
				pem: []byte("Bad pem"),
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := NewJWT(tt.args.rsa, tt.args.pem)
			dataToencript := map[string]interface{}{
				"user_id": "12345",
			}
			dataDecripted := map[string]interface{}{}
			token, timestamp, err := j.Create(dataToencript, 1)
			if err != nil {
				t.Fatal(err)
			}
			data, err := j.Validate(token)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateJWT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				jsonUsr, err := json.Marshal(data)
				if err != nil {
					t.Fatal(err)
				}
				err = json.Unmarshal(jsonUsr, &dataDecripted)
				if err != nil {
					t.Fatal(err)
				}

				if dataDecripted["user_id"] != dataToencript["user_id"] {
					t.Errorf("ValidateJWT() dataDecripted = %v, want %v", dataDecripted, dataToencript)
				}
				t.Log(dataDecripted)
				if timestamp < 10 {
					t.Errorf("ValidateJWT() timestamp = %v, want > %v", timestamp, 10)
				}
			}

		})
	}
}
