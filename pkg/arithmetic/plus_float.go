package arithmetic

func SumNullFloats(values ...*float64) *float64 {
	var result *float64
	for _, value := range values {
		if value == nil {
			continue
		}
		if result == nil {
			result = value
		} else {
			*result += *value
		}
	}
	return result
}
