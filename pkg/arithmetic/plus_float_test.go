package arithmetic

import (
	"testing"
)

func Test_Sum_Float_Null(t *testing.T) {
	type args struct {
		numberOne *float64
		numberTwo *float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "When I have two numbers and both have a value",
			args: args{
				numberOne: convertFloatToFloatPointer(30.5),
				numberTwo: convertFloatToFloatPointer(60.5),
			},
			want: 91,
		},
		{
			name: "When I have two numbers and both have a value the zero",
			args: args{
				numberOne: convertFloatToFloatPointer(0.0),
				numberTwo: convertFloatToFloatPointer(0.0),
			},
			want: 0,
		},
		{
			name: "When I have two numbers and ony one have a value",
			args: args{
				numberOne: convertFloatToFloatPointer(10),
				numberTwo: nil,
			},
			want: 10,
		},
	}
	type argsV2 struct {
		numberOne *float64
		numberTwo *float64
	}
	testsV2 := []struct {
		name string
		args argsV2
		want *float64
	}{
		{
			name: "When I have two numbers and both have null value",
			args: argsV2{
				numberOne: nil,
				numberTwo: nil,
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := SumNullFloats(tt.args.numberOne, tt.args.numberTwo)
			if *got != tt.want {
				t.Errorf("SumNullFloats() got %v and wantErr %v", got, tt.want)
			}

		})
	}
	for _, tt := range testsV2 {
		t.Run(tt.name, func(t *testing.T) {
			got := SumNullFloats(tt.args.numberOne, tt.args.numberTwo)
			if got != tt.want {
				t.Errorf("SumNullFloats() got %v and wantErr %v", got, tt.want)
			}

		})
	}
}

func convertFloatToFloatPointer(value float64) *float64 {
	return &value
}
