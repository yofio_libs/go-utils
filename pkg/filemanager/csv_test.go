package filemanager

import (
	"bytes"
	"reflect"
	"testing"
)

func Test_FormatCsv(t *testing.T) {
	type params struct {
		report [][]string
	}

	tests := []struct {
		name    string
		params  params
		want    []byte
		wantErr bool
	}{
		{
			name: "Given empty report When format csv Then return empty csv bytes",
			params: params{
				report: [][]string{},
			},
			want:    []byte(""),
			wantErr: false,
		},
		{
			name: "Given valid report When format csv Then return csv bytes",
			params: params{
				report: [][]string{
					{"fecha", "id_venta", "tipo_de_transaccion", "monto"},
					{"2018-01-01", "1", "1", "1"},
					{"2018-01-02", "2", "2", "2"},
				},
			},
			want: []byte(`fecha,id_venta,tipo_de_transaccion,monto` + "\n" +
				`2018-01-01,1,1,1` + "\n" +
				`2018-01-02,2,2,2` + "\n"),
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := FormatCsv(tt.params.report)
			if (err != nil) != tt.wantErr {
				t.Errorf("FormatCsv() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if !reflect.DeepEqual(got, tt.want) {
					if !bytes.Equal(got, tt.want) {
						t.Errorf("FormatCsv() = %v, want %v", string(got), string(tt.want))
					}
				}
			}
		})
	}
}
