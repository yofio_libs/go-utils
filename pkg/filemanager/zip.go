package filemanager

import (
	"bytes"
	"fmt"
	"io"
	"os"

	"github.com/alexmullins/zip"
)

func CompressFileWithPassword(fileName string, password string, contents []byte) ([]byte, error) {
	if fileName == "" {
		return nil, fmt.Errorf("file name is empty")
	}

	if password == "" {
		return nil, fmt.Errorf("password is empty")
	}

	if contents == nil {
		return nil, fmt.Errorf("contents is empty")
	}

	if string(contents) == "" {
		return nil, fmt.Errorf("contents is empty")
	}

	tempDir, err := os.MkdirTemp("", "yofio")
	if err != nil {
		return nil, err
	}
	zipFileName := tempDir + `/` + fileName + `.zip`
	defer func(path string) {
		_ = os.RemoveAll(path)
	}(tempDir)

	fzip, err := os.Create(zipFileName)
	if err != nil {
		return nil, err
	}
	zipw := zip.NewWriter(fzip)

	w, err := zipw.Encrypt(fileName, password)
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(w, bytes.NewReader(contents))
	if err != nil {
		return nil, err
	}

	err = zipw.Close()
	if err != nil {
		return nil, err
	}

	err = zipw.Flush()
	if err != nil {
		return nil, err
	}
	zipFile, err := os.ReadFile(zipFileName)
	if err != nil {
		return nil, err
	}

	return zipFile, nil
}
