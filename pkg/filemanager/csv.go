package filemanager

import (
	"bytes"
	"encoding/csv"
)

func FormatCsv(report [][]string) ([]byte, error) {
	var buffer bytes.Buffer
	writer := csv.NewWriter(&buffer)
	err := writer.WriteAll(report)
	if err != nil {
		return nil, err
	}

	if err := writer.Error(); err != nil {
		return nil, err
	}

	return buffer.Bytes(), nil
}
