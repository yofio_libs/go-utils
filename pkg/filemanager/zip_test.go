package filemanager

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

func Test_Zip(t *testing.T) {
	type args struct {
		fileName string
		password string
		contents []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "Given emtpy file name When compress file Then error",
			args: args{
				fileName: "",
				password: "",
				contents: []byte(""),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Given emtpy password When compress file Then error",
			args: args{
				fileName: "test",
				password: "",
				contents: []byte(""),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Given emtpy contents When compress file Then error",
			args: args{
				fileName: "test",
				password: "password",
				contents: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Given file name and password and contents When compress file Then success",
			args: args{
				fileName: "test",
				password: "password",
				contents: []byte("123"),
			},
			want:    []byte("123"),
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CompressFileWithPassword(tt.args.fileName, tt.args.password, tt.args.contents)
			if (err != nil) != tt.wantErr {
				t.Errorf("CompressFileWithPassword() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr {
				return
			}

			matches, err := filepath.Glob(os.TempDir() + "/yofio*")
			if err != nil {
				t.Errorf("CompressFileWithPassword() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(matches) > 0 {
				t.Error("CompressFileWithPassword() error = tmp file not deleted")
			}

			if reflect.DeepEqual(got, tt.want) {
				t.Errorf("CompressFileWithPassword() = %v, want %v", got, tt.want)
			}
		})
	}
}
