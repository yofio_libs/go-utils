package mathyofio

import "math"

func Round(num float64, nbDigits float64) float64 {
	pow := math.Pow(10., nbDigits)
	rounded := float64(int(num*pow)) / pow
	return rounded
}
