package mathyofio

import "testing"

func Test_Round(t *testing.T) {
	type args struct {
		num      float64
		nbDigits float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "Test_Round_1",
			args: args{
				num:      3.141592653589793,
				nbDigits: 2,
			},
			want: 3.14,
		},
		{
			name: "Test_Round_2",
			args: args{
				num:      3.141592653589793,
				nbDigits: 3,
			},
			want: 3.141,
		},
		{
			name: "Test_Round_3",
			args: args{
				num:      3.141592653589793,
				nbDigits: 4,
			},
			want: 3.1415,
		},
		{
			name: "Test_Round_4",
			args: args{
				num:      3.141592653589793,
				nbDigits: 5,
			},
			want: 3.14159,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Round(tt.args.num, tt.args.nbDigits); got != tt.want {
				t.Errorf("Round() = %v, want %v", got, tt.want)
			}
		})
	}
}
