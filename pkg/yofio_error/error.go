package yofioerror

import (
	"fmt"
)

type InternalErrorHandler interface {
	Error() string
	Message() string
	Code() int
	String() string
}

type InternalError struct {
	code            InternalErrorCode
	err             error
	readableMessage string
	httpStatusCode  int
	level           InternalErrorLevel
}

type InternalErrorCode int

const (
	NotFound InternalErrorCode = iota
	DatabaseConection
	DatabaseQuery
	InvalidEmailFormat
	InvalidEmailDomain
	InvalidAddress
)

type InternalErrorLevel int

const (
	Low InternalErrorLevel = iota
	Normal
	High
	Highest
)

func NewInternalError(err error, internalCode InternalErrorCode, message string, params ...interface{}) InternalError {
	ie := InternalError{err: err, code: internalCode, readableMessage: fmt.Sprintf(message, params...)}
	ie.level = ie.GetLevel()
	return ie
}

func (e InternalError) Error() string {
	return e.err.Error()
}

func (e InternalError) GetMessage() string {
	return e.readableMessage
}

func (e InternalError) GetCode() int32 {
	return int32(e.code)
}

func (e InternalError) String() string {
	return fmt.Sprintf("code:%v, readable_message:%s, error:%s, http_status_code: %d", e.code, e.readableMessage, e.err, e.httpStatusCode)
}

func (e InternalError) GetLevel() InternalErrorLevel {
	switch e.code {
	case NotFound:
		return Low
	case DatabaseConection:
		return Highest
	case DatabaseQuery:
		return High
	default:
		return Normal
	}
}
