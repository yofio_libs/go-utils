PACKAGES := $(go list ./... | grep -v node_modules | grep -v cmd | grep -v examples | grep -v test)

.PHONY: test

only-test: ## Only execute all tests (no mock generation, no linter)
	go test -v `go list ./... | grep -v cmd | grep -v restapi | grep -v models | grep -v mocks`

upgrade-deps: ## Upgrade all dependencies
	@go get -u -t ./...
	@go mod tidy

test: only-test linter## Execute all tests
	@echo "Test run successfully"

test-cover: ## Runs all the test files reporting coverage
	go test -v -count=1 -timeout 30s -coverprofile=test-reports/coverage.out -covermode=atomic `go list ./... | grep -v cmd | grep -v restapi | grep -v models | grep -v mocks`
	#go tool cover -func test-reports/coverage.out | grep total | awk '{print substr($3, 1, length($3)-1)}'
	go tool cover -html=test-reports/coverage.out -o test-reports/coverage.html

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36mmake %-15s\033[0m %s\n", $$1, $$2}'

linter: ## Executes the linter for reviewing the code health
	@echo "Executing linter for golang"
	@golangci-lint run
	@echo "Linter successfully passed"

